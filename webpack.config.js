const fs = require('fs');
const webpack = require('webpack');
const packageJSON = require('./package.json');
const ClosureCompiler = require('google-closure-compiler-js').webpack;


module.exports = {
  entry: ['./src/main.js', './src/mockConfig.js'],
  output: {
    path: `${__dirname}/dist`,
    filename: 'morpheus.min.js',
  },
  plugins: [
    new webpack.DefinePlugin({
      SBI_KM_VERSION: JSON.stringify(packageJSON.version),
    }),
    new ClosureCompiler({
      options: {
        env: 'CUSTOM',
        languageIn: 'ECMASCRIPT6',
        languageOut: 'ECMASCRIPT5',
        compilationLevel: 'ADVANCED',
        rewritePolyfills: true,
        externs: [{ src: fs.readFileSync(`${__dirname}/externs.js`, 'utf8') }],
        assumeFunctionWrapper: true,
        warningLevel: 'VERBOSE',
        outputWrapper: `/* Sonobi - Morpheus version: ${packageJSON.version} */\n(function(){%output%}).call(window)`,
      },
      concurrency: 3,
    }),
  ],
};
