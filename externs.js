/**
 * @fileoverview JavaScript Built-Ins for googletag properties.
 *
 * @externs
 * @author ronald@sonobi.com (Ronald Berner)
 */

let HTMLElement;

const window = {
  performance: {
    timing: {
      navigationStart: '',
    },
    now() {},
  },
  console() {},
  location: {
    href: '',
  },
  setTimeout() {},
  setInterval() {},
  clearInterval() {},
  clearTimeout() {},
  addEventListener(message, callback) {
    const event = {
      source: {},
      data: {},
    };
    callback(event);
  },
  XMLHttpRequest() {
    return {
      responseType: '',
      open() {},
      setRequestHeader() {},
      send() {},
    };
  },
  XDomainRequest() {
    return {
      responseText: '',
    };
  },
  document: {
    referrer: '',
    location: {
      href: '',
      protocol: '',
      hostname: '',
      origin: '',
    },
    getElementsByTagName() {
      return [{
        insertBefore() {},
        firstChild: {},
      }];
    },
    createElement() {
      return {
        readyState: '',
        contentWindow: {
          document: {
            open() {},
            write() {},
            close() {},
          },
          postMessage() {},
          open() {},
          write() {},
          close() {},
        },
      };
    },
    getElementsByName() {},
    cookie: '',
    documentElement: {
      clientWidth: '',
      clientHeight: '',
    },
    body: {
      clientWidth: '',
      clientHeight: '',

      offsetHeight: '',
      offsetWidth: '',
    },
    head: {
      appendChild() {},
    },
  },
  screen: {
    availWidth: '',
    availHeight: '',
  },
  innerWidth: '',
  innerHeight: '',
};
let Image;
