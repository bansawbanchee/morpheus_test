/* eslint quotes: ["off"] */
export default {
  adServer: 'dfp',
  url: 'http://mtrx.go.sonobi.com/morpheus.js',
  analyticsSettings: {
    url: 'https://keymaker.go.sonobi.com/keymaker',
    timeout: 2000,
  },
  globalSettings: {
    sizes: ['728x90', '300x250', '300x600'],
    timeout: 1600,
  },
  platformSettings: {
    mobile: {
      viewport: {
        lt: 768,
      },
      timeout: 1600,
    },
    tablet: {
      viewport: {
        lt: 992,
        ge: 768,
      },
      timeout: 1600,
    },
    desktop: {
      viewport: {
        ge: 992,
      },
      timeout: 1600,
    },
  },
  adapterSettings: [{
    name: 'sonobi',
    disabled: false,
    priceRules: [{
      ge: 5000,
      set: 5000,
    }, {
      lt: 5000,
      ge: 3000,
      round: 10,
    }, {
      lt: 3000,
      ge: 900,
      round: 5,
    }, {
      lt: 900,
      ge: 0,
      round: 1,
    }],
    sizes: ['300x250'],
    platforms: ['mobile', 'tablet', 'desktop'],
    requestSettings: [{
      value: '1243675',
      name: 'placementid',
      floor: [0.01, 10],
      dimensions: {
        sizes: ['300x250'],
        adUnits: [],
        domains: [],
        platforms: ['desktop', 'mobile', 'tablet'],
      },
    }, {
      value: 'someHfaKey',
      name: 'hfa',
      floor: [0.01, 10],
      dimensions: {
        sizes: ['300x250'],
        adUnits: [],
        domains: [],
        platforms: ['desktop', 'mobile', 'tablet'],
      },
    }, {
      value: "my.special.cdf",
      name: "cdf",
      floor: [0.01, 10],
      dimensions: {
        sizes: ["300x250"],
        adUnits: [],
        domains: [],
        platforms: ['desktop', "mobile", 'tablet'],
      },
    }, {
      value: "appnexus_params",
      name: "ant",
      floor: [],
      dimensions: {
        sizes: ["300x250"],
        adUnits: [],
        domains: [],
        platforms: ['desktop', "mobile", 'tablet'],
      },
    },
    ],
  }, {
    name: 'amazon',
    disabled: false,
    timeout: 1500,
    sizes: [
      '970x250',
      '728x90',
      '160x600',
      '32050',
      '300x250',
      '300x600',
    ],
    requestSettings: [
      {
        name: 'debugURL',
        value: 'someAmazonURL',
        dimensions: {
          platforms: [],
          domains: ['*'],
        },
      },
      {
        name: 'id',
        value: 'someAmazonID',
        dimensions: {
          platforms: [],
          domains: ['*'],
        },
      },
    ],
    parseSettings: [
      {
        name: 'step',
        value: '0',
        dimensions: {
          platforms: [],
          domains: ['*'],
        },
      },
      {
        name: 'ceil',
        value: '100',
        dimensions: {
          platforms: [],
          domains: ['*'],
        },
      },
      {
        name: '10',
        value: '0x0',
        dimensions: {
          platforms: [],
          domains: ['*'],
        },
      },
      {
        name: '3x6',
        value: '300x600',
        dimensions: {
          platforms: [],
          domains: ['*'],
        },
      },
      {
        name: '1x6',
        value: '100x600',
        dimensions: {
          platforms: [],
          domains: ['*'],
        },
      },
      {
        name: '3x2',
        value: '300x250',
        dimensions: {
          platforms: [],
          domains: ['*'],
        },
      },
      {
        name: '7x9',
        value: '728x90',
        dimensions: {
          platforms: [],
          domains: ['*'],
        },
      },
    ],
  }, {
    name: 'bRealTime',
    disabled: false,
    sizes: [
      '728x90',
      '970x250',
      '300x250',
      '300,600',
    ],
    platforms: ['desktop', 'mobile', 'tablet'],
    requestSettings: [
      {
        name: 'placementId',
        value: '0307',
        dimensions: {
          sizes: [
            '728x90',
          ],
          adUnits: [],
          domains: [],
          platforms: ['mobile', 'tablet', 'desktop'],
        },
      },
      {
        name: 'placementId',
        value: '709',
        dimensions: {
          sizes: [
            '970x250',
          ],
          adUnits: [],
          domains: [],
          platforms: ['mobile', 'tablet', 'desktop'],
        },
      },
      {
        name: 'placementId',
        value: '24902',
        dimensions: {
          sizes: [
            '728x90',
          ],
          adUnits: ['slotId'],
          domains: [],
          platforms: ['mobile', 'tablet', 'desktop'],
        },
      },
      {
        name: 'placementId',
        value: '7307',
        dimensions: {
          sizes: [
            '728x90',
            '970x250',
            '300x250',
            '300,600',
          ],
          adUnits: ['*'],
          domains: [],
          platforms: ['mobile', 'tablet', 'desktop'],
        },
      },
    ],

  }, {
    name: 'indexExchange',
    disabled: false,
    sizes: ['0x0'],
    requestSettings: [
      {
        value: 'somePageUrl.com',
        name: 'pageUrl',
      },
      {
        value: 'indexSiteId',
        name: 'siteId',
        platforms: ['desktop'],
        dimensions: {
          sizes: ['0x0'],
        },
      },
      {
        value: 'indexSiteId',
        name: 'siteId',
        platforms: ['mobile'],
        dimensions: {
          sizes: ['0x0'],
        },
      },
      {
        value: 'indexSiteId',
        name: 'siteId',
        platforms: ['tablet'],
        dimensions: {
          sizes: ['0x0'],
        },
      },
    ],
  }, {
    name: 'openx',
    disabled: false,
    priceRules: [{
      ge: 5000,
      set: 5000,
    }, {
      lt: 5000,
      ge: 3000,
      round: 10,
    }, {
      lt: 3000,
      ge: 900,
      round: 5,
    }, {
      lt: 900,
      ge: 0,
      round: 1,
    }],
    sizes: ['720x2'],
    platforms: ['mobile', 'desktop'],
    requestSettings: [{
      name: 'serverUrl',
      value: 'domai.nr',
    },
    {
      value: 'network_code',
      name: 'networkCode',
      floor: [0.01, 10],
      dimensions: {
        sizes: [],
        adUnits: [],
        domains: [],
        platforms: [],
      },
    }, {
      value: 'a place for things',
      name: 'PLAceMenTID',
      floor: [0.01, 10],
      dimensions: {
        sizes: ['720x2'],
        adUnits: ['/10'],
        domains: [],
        platforms: ['desktop', 'tablet', 'mobile'],
      },
    }],
    parseSettings: [],
  }, {
    name: 'pubmatic',
    timeout: 1200,
    disabled: false,
    sizes: ['728x90', '300x250', '300x600'],
    platforms: ['mobile', 'desktop', 'tablet'],
    requestSettings: [{
      value: '32572',
      name: 'publisherId',
      dimensions: {
        sizes: [],
        adUnits: [],
        domains: [],
        platforms: [],
      },
    }],
  }, {
    name: 'rubicon',
    disabled: false,
    priceRules: [
      {
        ge: 5000,
        set: 5000,
      },
      {
        lt: 5000,
        ge: 3000,
        round: 10,
      },
      {
        lt: 3000,
        ge: 900,
        round: 5,
      },
      {
        lt: 900,
        ge: 0,
        round: 1,
      },
    ],
    sizes: [
      '300x250',
    ],
    platforms: [
      'mobile',
      'desktop',
    ],
    requestSettings: [
      {
        value: 'baserUrl',
        name: 'optimized-by.rubiconproject.com/a/api/fastlane.jsonp',
      },
      {
        value: '8526',
        name: 'accountId',
      },
      {
        value: '90172',
        name: 'siteId',
      },
      {
        value: '424714',
        name: '',
        dimensions: {
          sizes: ['728x90', '970x90'],
          adUnits: ['/blah/Leaderboard_970x90_ATF'],
        },
      },
      {
        value: '424716',
        name: '',
        dimensions: {
          sizes: ['300x600', '300x250'],
          adUnits: ['/blah/H_I_300x250'],
        },
      },
      {
        value: '424718',
        name: '',
        dimensions: {
          sizes: ['160x600'],
          adUnits: ['/blah/H_I_160x600'],
        },
      },
    ],
  }, {
    name: 'sovrn',
    disabled: false,
    priceRules: [{
      ge: 5000,
      set: 5000,
    }, {
      lt: 5000,
      ge: 3000,
      round: 10,
    }, {
      lt: 3000,
      ge: 900,
      round: 5,
    }, {
      lt: 900,
      ge: 0,
      round: 1,
    }],
    sizes: ['720x2'],
    platforms: ['mobile', 'desktop'],
    requestSettings: [{
      value: 'tagId',
      name: 'placementId',
      floor: [1],
      dimensions: {
        sizes: ['0x0'],
        adUnits: [],
        domains: [],
        platforms: [],
      },
    }],
    parseSettings: [],
  },
  {
    requestSettings: [
      {
        value: "372317879785973_380465335637894",
        name: "placementid",
        dimensions: {
          sizes: ["320x50"],
        },
      },
      {
        value: "372317879785973_387024611648633",
        name: "placementid",
        dimensions: {
          sizes: ["300x250"],
        },

      },
      {
        name: "testmode",
        value: "true",
      },
      {
        name: "pageurl",
        value: "test.go.sonobi.com/fb.html",
      },
    ],
    platforms: [
      "mobile",
      "desktop",
      "tablet",
    ],
    sizes: [
      "320x50",
      "300x250",
    ],

    timeout: 2500,
    disabled: false,
    name: "audienceNetwork",
  },
  {
    name: 'yieldbot',
    timeout: 1200,
    disabled: false,
    sizes: ['728x90', '300x250', '300x600'],
    platforms: ['mobile', 'desktop', 'tablet'],
    // Names for the slots probably won't be used,
    // so I'm copying the slot name for readability
    requestSettings: [{
      name: 'leaderboard',
      dimensions: {
        adUnits: ['leaderboard'],
        sizes: ['728x90'],
      },
    }, {
      name: 'medrec',
      dimensions: {
        adUnits: ['medrec'],
        sizes: ['300x250', '300x600'],
      },
    }, {
      name: 'medrec_BTF',
      dimensions: {
        adUnits: ['medrec_BTF'],
        sizes: ['300x250'],
      },
    }, {
      value: '5cfa',
      name: 'pub',
    }],
  }, {
    requestSettings: [
      {
        name: "serverUrl",
        value: 'adserver-us.adtech.advertising.com',
      },
      {
        name: "network",
        value: "10742.1",
      },
      {
        name: "placementId",
        value: "4260868",
        dimensions: {
          adUnits: ["/84313249/HistoryThings_Leaderboard_Top"],
        },
      },
      {
        name: "placementId",
        value: "4260869",
        dimensions: {
          adUnits: ["/84313249/HistoryThings_Leaderboard2"],
        },
      },
      {
        name: "placementId",
        value: "4260870",
        dimensions: {
          adUnits: ["/84313249/HistoryThings_Leaderboard3"],
        },
      },
      {
        name: "placementId",
        value: "4260871",
        dimensions: {
          adUnits: ["/84313249/HistoryThings_Rightrail2"],
        },
      },
    ],
    platforms: [
      "mobile",
      "desktop",
      "tablet",
    ],
    sizes: [
      "300x250",
      "300x600",
      "300x1050",
      "300x50",
      "320x50",
      "320x100",
      "728x90",
      "160x600",
      "120x600",
      "970x250",
      "970x90",
      "300x100",
      "970x66",
    ],
    timeout: 2500,
    disabled: false,
    name: "aol",
  }, {
    name: "c1x",
    disabled: false,
    sizes: [
      "300x250",
      "300x600",
      "728x90",
    ],
    requestSettings: [
      {
        name: "endpoint",
        value: "ht.c1exchange.com:9000/ht",
      },
      {
        name: "siteId",
        value: "1",
      },
      {
        name: "dspid",
        value: "OPTIONAL-heh",
      },
      {
        name: "pixelId",
        value: "999999",
      },
      {
        name: "pixeldelay",
        value: "1500",
      },
      {
        name: "pixelendpoint",
        value: "px.c1exchange.com/pubpixel/",
      },
      {
        name: "bidfloormap",
        value: "4",
        dimensions: {
          sizes: ["300x250"],
        },
      },
      {
        name: "bidfloormap",
        value: "3.5",
        dimensions: {
          sizes: ["300x600"],
        },
      },
      {
        name: "bidfloormap",
        value: "6",
        dimensions: {
          sizes: ["728x90"],
        },
      },
    ],
  }],
};
